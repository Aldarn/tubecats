# Installation #

1. Clone the repository somewhere locally.

2. Navigate to the cloned repository and create a new virtual environment using `virtualenv wrapper` if installed:

        mkvirtualenv tube

    Otherwise use standard `virtualenv`:

        virtualenv tube
        source tube/bin/activate

3. Install the requirements using `pip`:

        pip install -r requirements.txt
        

# Running #

Run `python tube.py` from the base repository directory.


# Testing #

There are 61 tests in total, mostly unit tests with a few integration tests.

Run `nosetests` from the base repository directory.


# Assumptions #

1. The average number of moves for an owner to find their cat has been taken as a sum of the number of moves 
for each successful reunion divided by the total number of reunions, where the number of reunion moves is 
defined as the sum of the number of successful moves by the cat and owner up to the point of reunion. 

2. Once humans have visited all stations they will also move randomly like cats.


# Discussion #

The simulation appears to be quite slow when increasing the number of movables. A slowdown is obviously to be expected, 
however I haven't profiled the simulation and as such i'm sure there are many optimizations that could be made, beyond 
the simple ones already built into the game logic and data structures.

A game is considered a SUCCESS if all cats are found, and a FAILURE otherwise. This is indicated in the end game stats.

The algorithm for determining the movement of an owner has been improved from simply visiting a station not visited 
before to traversing the tube map, backtracking when there are no more new stations to traverse to the last point where 
they had an option of multiple new stations. This continues until all possible stations are reached (accounting for 
any stations closed and blocked by cats and owners being reunited), at which point the owner moves randomly like the 
cat does.

Part of the station creation code includes a binary search algorithm responsible for generating labels for the stations. 
There is some discussion in the docstrings about how this works and other algorithms I tried, however it doesn't explain 
why I did it in the first place. The intention here was, as I go into more detail below, to render the simulation in 
real time, which would have required labels rather than long full names to identify the stations. As it turned out one 
of my previous implementations that _didn't_ require each label be the same length might have been superior as some 
of the tube names start with long matching strings.

I was intending to draw the graph using networkx and matplotlib, however I ran out of time (I blame the football and 
brexit!). This is some scratch code I threw together to prove the concept, in theory you could substitute the tube 
graph here and then call the update method each game loop and you would have a somewhat working visual simulation. Some 
work would then be needed to annotate the nodes with their labels and indications of if they're closed, along with 
showing where the cats and owners are on the map. Maybe i'll do it in my spare time this week :)


        import numpy as np
        import matplotlib.pyplot as plt
        import networkx as nx
        import matplotlib as mpl
        from matplotlib import animation
        import random
        
        G = nx.Graph()
        G.add_edges_from([(0, 1), (1, 2), (2, 0)])
        fig = plt.figure(figsize=(8, 8))
        from networkx.drawing.nx_agraph import graphviz_layout
        
        pos = graphviz_layout(G)
        nc = np.random.random(3)
        nodes = nx.draw_networkx_nodes(G, pos, node_color=nc)
        edges = nx.draw_networkx_edges(G, pos)
        
        def update(n):
            nc = np.random.random(3)
            nodes.set_array(nc)
            return nodes,
   
        anim = animation.FuncAnimation(fig, update, interval=50, blit=True)
        
        anim.save('basic_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
        
        plt.show()


Finally it has _just_ come to my attention that all the files appear to be indented with tabs, even though I configured 
PyCharm to use spaces for this project several times! It appears there's a bug in the IDE. I would convert them all to 
spaces in order to conform to PEP8 however this would incur a mass commit which I don't think is worth it at this point, 
apologies in advance!