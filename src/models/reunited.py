class Reunited(object):
	"""
	Object representing a reunited cat and owner with some additional statistics on their journey.
	"""

	def __init__(self, movable, movable_partner, game_ticks_taken):
		self.movable = movable
		self.movable_partner = movable_partner
		self.game_ticks_taken = game_ticks_taken

	def __str__(self):
		return u"{movable} <3 {movable_partner} (Took {game_ticks_taken} ticks!)".format(
			movable=self.movable.name,
			movable_partner=self.movable_partner.name,
			game_ticks_taken=self.game_ticks_taken
		)

	def __repr__(self):
		return "Reunited(\"{movable}\", \"{movable_partner}\", \"{game_ticks_taken}\")".format(
			movable=self.movable.__repr__(),
			movable_partner=self.movable_partner.__repr__(),
			game_ticks_taken=self.game_ticks_taken
		)
