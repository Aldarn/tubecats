from weakref import WeakSet


class Station(object):
	"""
	Represents a station on the tube map, holding references to any movables
	currently at this station.
	"""

	def __init__(self, id, name, label):
		self.id = id
		self.name = name
		self.label = label
		self.open = True
		self.movables = WeakSet()  # Use a weak set to ensure objects discarded don't remain in the set

	def add_movable(self, movable):
		"""
		Adds a movable to the station and changes the movable's station reference
		to this one.

		:param movable: The movable to add to this station.
		"""
		self.movables.add(movable)

		# Ensure the movable has the new station if not already set
		if movable.station != self:
			movable.station = self

	def remove_movable(self, movable):
		"""
		Removes a movable from this station and unassigns the movable's station reference.

		:param movable: The movable to remove.
		"""
		self.movables.discard(movable)

		# Ensure the movable has its station reference unassigned
		movable.station = None

	def __str__(self):
		return "#{id} {name} ({label}) - Open: {open}".format(
			id=self.id,
			name=self.name,
			label=self.label,
			open=self.open
		)

	def __repr__(self):
		return "Station(\"{id}\", \"{name}\", \"{label}\")".format(
			id=self.id,
			name=self.name,
			label=self.label
		)
