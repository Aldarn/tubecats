import random
from weakref import ref

from movable import Movable


class Cat(Movable):
	"""
	Object representing the owner of a cat, providing methods to perform
	movement each game tick.
	"""

	def __init__(self, id, name, station):
		super(Cat, self).__init__(station)

		self.id = id
		self.name = name
		self.owner = None

	@property
	def owner(self):
		return self._owner() if self._owner is not None else None

	@owner.setter
	def owner(self, owner):
		self._owner = ref(owner) if owner is not None else None

		# Ensure the owner's cat is set to this one if it's not already
		if owner is not None and owner.cat != self:
			owner.cat = self

	def move(self, tube_map):
		"""
		Moves the cat to a random station connected to its current station, if open.

		:param tube_map: The graph representation of the tube map.
		:return: True if the cat successfully moves, false otherwise.
		"""
		# Get all connected stations that are open
		neighbors = filter(lambda station: station.open, tube_map.neighbors(self.station))

		# Set the new station to a random connected station if there are any left open
		if neighbors:
			self.station = neighbors[random.randrange(len(neighbors))]
			return True
		return False

	@property
	def partner(self):
		return self.owner

	def __str__(self):
		return "#{id} {name} - Owner: {owner}".format(
			id=self.id,
			name=self.name,
			owner=self.owner.name if self.owner else None
		)

	def __repr__(self):
		return "Cat(\"{id}\", \"{name}\", \"{station}\")".format(
			id=self.id,
			name=self.name,
			station=self.station.__repr__()
		)
