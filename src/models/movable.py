import abc
from weakref import ref


class Movable(object):
	"""
	Interface describing common methods for movable objects on the map.
	"""
	__metaclass__ = abc.ABCMeta

	def __init__(self, station):
		self.move_count = -1  # -1 since the initial placing at a station increments the count
		self.station = station

	@property
	def station(self):
		return self._station() if self._station is not None else None

	@station.setter
	def station(self, station):
		self.move_count += 1
		self._station = ref(station) if station is not None else None

		# Ensure this movable is added to the station movables list if not already in it
		if station is not None and self not in station.movables:
			station.add_movable(self)

	@abc.abstractmethod
	def move(self, tube_map):
		"""
		Abstract method for each movable to define how to move itself within the given tube map.

		:param tube_map: Graph representation of the tube map.
		"""
		pass

	@property
	@abc.abstractmethod
	def partner(self):
		"""
		Returns the partner this movable is trying to reunite with.

		:return: Movable to reunite with.
		"""
		pass

	@property
	def reunited(self):
		"""
		Determines if two movables belonging to each other have met at the same station, and
		closes the station if so.

		:return: True or False
		"""
		return self.station == self.partner.station
