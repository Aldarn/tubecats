import random

from weakref import WeakSet, ref

from movable import Movable


class Owner(Movable):
	"""
	Object representing the owner of a cat, providing methods to perform
	movement each game tick.
	"""

	def __init__(self, id, name, station):
		super(Owner, self).__init__(station)

		self.id = id
		self.name = name
		self.cat = None

		# Private variables used during movement
		self._visited_stations = WeakSet({station})
		self._move_generator = None

	@property
	def cat(self):
		return self._cat() if self._cat is not None else None

	@cat.setter
	def cat(self, cat):
		self._cat = ref(cat) if cat is not None else None

		# Ensure the cat owner is set to this one if it's not already
		if cat is not None and cat.owner != self:
			cat.owner = self

	def _get_open_neighbors(self, tube_map):
		"""
		Gets a list of all the open stations connected to the owners current station.

		:param tube_map: The map of stations.
		:return: List of open stations.
		"""
		return filter(
			lambda station: station.open,
			tube_map.neighbors(self.station)
		)

	def _get_new_neighbors(self, neighbors):
		"""
		Filters a list of neighbors to a list of unvisited neighbors.

		:param neighbors: The neighbors to filter.
		:return: List of unvisited neighbors.
		"""
		return filter(
			lambda station: station not in self._visited_stations,
			neighbors
		)

	def move(self, tube_map):
		"""
		Moves the owner by using the best available mode depending on the current owner station.

		:param tube_map: The tube map to move the owner around.
		:return: True if the move is successful, False otherwise.
		"""
		if self._move_generator:
			try:
				return next(self._move_generator)
			except SuperStopIteration:
				# This happens when we either get trapped or when backtracking fails because a
				# station was closed, in this case we fallback to random moving until we
				# potentially find a new unvisited station
				pass
			except StopIteration:
				# This happens when we exhaust the available unvisited stations to check
				pass

		neighbors = self._get_open_neighbors(tube_map)
		new_neighbors = self._get_new_neighbors(neighbors)

		# No neighbours means this owner is stuck, fail the move
		if not neighbors:
			return False

		# If there's new neighbors to explore then use smart move
		if new_neighbors:
			self._move_generator = self._smart_move(tube_map, neighbors=neighbors, new_neighbors=new_neighbors)
			return next(self._move_generator)

		# Fallback to random move until we find new ones
		return self._random_move(neighbors)

	def _smart_move(self, tube_map, neighbors=None, new_neighbors=None):
		"""
		Moves the owner to an open connected unvisited station from their starting point, backtracking
		when no more open stations are found, returning to the point where they previously had a choice
		of open stations, if possible.

		This is essentially a depth first search algorithm that attempts to move to every unvisited
		station efficiently, remembering points of branching. It has been modified to take into account
		stations being closed along the journey.

		:param tube_map: The tube map to move around.
		:param neighbors: Predefined list of neighbors.
		:param new_neighbors: Predefined list of new neighbors.
		:return: True if successfully moves, False otherwise.
		"""
		# Grab the neighbours if not provided
		if neighbors is None:
			neighbors = self._get_open_neighbors(tube_map)

		# No open neighbours, we can't move, end generation
		if not neighbors:
			raise SuperStopIteration()

		# Filter the neighbours by ones we haven't yet visited
		if not new_neighbors:
			new_neighbors = self._get_new_neighbors(neighbors)

		# If no new neighbours then we begin backtracking
		if not new_neighbors:
			return

		# Explore all new neighbours recursively
		for new_neighbor in new_neighbors:
			# Check this new neighbor is still open since several game moves could have happened
			# since we generated this list
			if not new_neighbor.open:
				continue

			# Set the previous new station reference so we can backtrack if we run out of options
			previous_new_station = self.station

			# Move to the new station
			self.station = new_neighbor
			self._visited_stations.add(self.station)

			# Pause here and return a successful move
			yield True

			# Continue from this point with a new recursion
			for result in self._smart_move(tube_map):
				if result:
					yield result

			# Recursive move has found no more new neighbours, now backtrack if possible
			if previous_new_station.open:
				self.station = previous_new_station
				yield True

			# Backtrack fails as a station we previously visited has now closed, exit smart move algorithm
			else:
				raise SuperStopIteration()

	def _random_move(self, neighbors):
		"""
		Moves the owner to a random station in the given neighbors. This is used when either the smart move
		fails because a station it wanted to backtrack to has been closed, or when all options have been
		exhausted.

		:param neighbors: The neighbors to pick from to move to.
		"""
		self.station = neighbors[random.randrange(len(neighbors))]
		return True

	@property
	def partner(self):
		return self.cat

	def __str__(self):
		return "#{id} {name} - Cat: {cat}".format(
			id=self.id,
			name=self.name,
			cat=self.cat.name if self.cat else None
		)

	def __repr__(self):
		return "Owner(\"{id}\", \"{name}\", \"{station}\")".format(
			id=self.id,
			name=self.name,
			station=self.station.__repr__()
		)


class SuperStopIteration(Exception):
	"""
	Exception to be used to stop a recursive generator iteration since StopIteration is only
	applied at the singular generator level.
	"""
	pass
