import networkx as nx


class GraphService(object):
	"""
	Responsible for creating the graph representation of the tube map from
	the given stations and connections.
	"""

	def __init__(self):
		pass

	def build_graph(self, edges):
		graph = nx.Graph()
		graph.add_edges_from(edges)
		return graph


graph_service = GraphService()
