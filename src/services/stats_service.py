import os
import json

from resources.resources import getResourcePath


class StatsService(object):
    """
    Generates stats at the end of games and saves them in flat files to
    enable generating stats across multiple games.

    In a real world scenario this would be a good candidate for storing
    in a database.
    """
    def __init__(self):
        pass

    def _save_stats(self, stats, file_path='stats.json'):
        """
        Calculates the new global stats from the given stats and saves them to the stats file.

        :param stats: The game stats.
        :param file_path: The global stats file path.
        """
        global_stats = self._load_stats()

        # Calculate new stats
        if global_stats:
            global_stats["movables_count"] = global_stats["movables_count"] + stats["movables_count"]
            global_stats["reunited_count"] = global_stats["reunited_count"] + stats["reunited_count"]
            global_stats["average_find_moves"] = (global_stats["average_find_moves"] + stats["average_find_moves"]) / 2
            global_stats["trapped_count"] = global_stats["trapped_count"] + stats["trapped_count"]
            global_stats["game_ticks"] = global_stats["game_ticks"] + stats["game_ticks"]
            if stats["success"]:
                global_stats["success_count"] += 1
            else:
                global_stats["failure_count"] += 1
        else:
            global_stats = dict(stats)
            del global_stats["success"]
            global_stats["success_count"] = int(stats["success"])
            global_stats["failure_count"] = int(not stats["success"])

        with open(getResourcePath(file_path), 'w+') as stats_file:
            json.dump(global_stats, stats_file)

    def _load_stats(self, file_path='stats.json'):
        """
        Loads the global stats from the stats file, creating it if it doesn't exist.

        :param file_path: The global stats file path.
        :return: The loaded stats dictionary.
        """
        with touch_open(getResourcePath(file_path), 'r') as stats_file:
            try:
                return json.load(stats_file)
            except ValueError:
                return None

    def _generate_game_stats(self, movables_count, reunited, trapped, game_ticks):
        """
        Generates the game stats from the given data.

        :param movables_count: The number of movables in the game.
        :param reunited: The list of reunited cats and owners.
        :param trapped: The list of trapped cats and owners.
        :param game_ticks: The number of game ticks taken.
        :return: The generated game stats.
        """
        reunited_move_counts = [reunited_instance.movable.move_count + reunited_instance.movable_partner.move_count
                                for reunited_instance in reunited]

        game_stats = {
            "movables_count": movables_count,
            "reunited_count": len(reunited),
            "average_find_moves": sum(reunited_move_counts) / len(reunited) if reunited else -1,
            "trapped_count": len(trapped),
            "game_ticks": game_ticks,
            "success": len(reunited) == movables_count
        }

        # Save them
        self._save_stats(game_stats)

        return game_stats

    def output_stats(self, movables_count, reunited, trapped, game_ticks):
        """
        Takes the data from the game, generates the stats and outputs them onto the stdout
        in a human readable format, along with the current running global stats.

        :param movables_count: The number of movables in the game.
        :param reunited: The list of reunited cats and owners.
        :param trapped: The list of trapped cats and owners.
        :param game_ticks: The number of game ticks taken.
        """
        game_stats = self._generate_game_stats(movables_count, reunited, trapped, game_ticks)
        global_stats = self._load_stats()

        print "\n{bar} Game Results - {success} {bar}\n".format(
            bar="-" * 20,
            success="SUCCESS" if game_stats["success"] else "FAILURE"
        )
        print "Total number of cats: {movables_count}".format(movables_count=game_stats["movables_count"])
        print "Number of cats found: {reunited_count}".format(reunited_count=game_stats["reunited_count"])
        print "Average number of movements required to find a cat: {average_find_moves}".format(
            average_find_moves=game_stats["average_find_moves"]
        )
        print "Trapped cats and owners: {trapped_count}".format(trapped_count=game_stats["trapped_count"])
        print "Game ticks: {game_ticks}\n".format(game_ticks=game_stats["game_ticks"])
        print "-" * 64

        print "\n\n{bar} All Game Results {bar}\n\n".format(bar="-" * 23)
        print "SUCCESS {success_count} vs {failure_count} FAILURE".format(
            success_count=global_stats["success_count"],
            failure_count=global_stats["failure_count"],
        )
        print "Total number of cats: {movables_count}".format(movables_count=global_stats["movables_count"])
        print "Number of cats found: {reunited_count}".format(reunited_count=global_stats["reunited_count"])
        print "Average number of movements required to find a cat: {average_find_moves}".format(
            average_find_moves=global_stats["average_find_moves"]
        )
        print "Trapped cats and owners: {trapped_count}".format(trapped_count=global_stats["trapped_count"])
        print "Game ticks: {game_ticks}\n".format(game_ticks=global_stats["game_ticks"])
        print "-" * 64


def touch_open(filename, *args, **kwargs):
    """
	Allows opening of a file in read mode, creating it if it doesn't exist.

	Credit to http://stackoverflow.com/a/10352231
    """
    # Open the file in R/W and create if it doesn't exist. *Don't* pass O_TRUNC
    fd = os.open(filename, os.O_RDWR | os.O_CREAT)

    # Encapsulate the low-level file descriptor in a python file object
    return os.fdopen(fd, *args, **kwargs)
