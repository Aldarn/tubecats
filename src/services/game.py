from map_service import map_service
from stats_service import StatsService
from ..factories.cat_and_owner_factory import CatAndOwnerFactory
from ..models.reunited import Reunited
from ..models.owner import Owner
from ..models.cat import Cat


class Game(object):
	"""
	Performs the main game loop.
	"""
	MAX_TICKS = 100000

	def __init__(self, stations_json_file_path, connections_json_file_path, number_of_cats_and_owners):
		"""
		Initializes the game, loads the graph representation of the map and loads the number of cats and owners
		specified into the map.

		:param stations_json_file_path: The path to the json file containing the stations.
		:param connections_json_file_path: The path to the json file containing the connections between the stations.
		:param number_of_cats_and_owners: The number of cats and owners to load into the map.
		"""
		self.ticks = 0
		self.number_of_cats_and_owners = number_of_cats_and_owners
		self.tube_map = map_service.load_map(stations_json_file_path, connections_json_file_path)
		self.movables = CatAndOwnerFactory(self.tube_map).create_cat_and_owners(number_of_cats_and_owners)
		self.reunited = []
		self.trapped = []

	def run(self):
		"""
		Runs each tick of the game, moving all movables and processing any reunited cats and dogs.
		"""
		while self.ticks < self.MAX_TICKS:
			# Keep a list of reunited movables to skip if their partner already processed the reunion,
			# they will be gone from the list in the next tick.
			skip_reunited = []

			# Move all movables one by one, handling reunited movables on the fly
			# We are careful to take a copy of the list when iterating to ensure we can safely remove
			# members mid-iteration.
			for movable in self.movables[:]:
				# Skip any movables already reunited this tick
				if movable in skip_reunited:
					continue

				# Move the movable, handling any that fail to move and are thus trapped
				if not movable.move(self.tube_map):
					self._handle_trapped(movable)

				# Handle reunited movables if applicable
				if movable.reunited:
					self._handle_reunited(movable)
					skip_reunited.append(movable.partner)

			# Increment the game tickets
			self.ticks += 1

			# Determine if all movables have been reunited or trapped and end the game if so
			if not self.movables:
				break

		StatsService().output_stats(
			self.number_of_cats_and_owners,
			self.reunited,
			self.trapped,
			self.ticks
		)

	def _handle_trapped(self, movable):
		"""
		Adds trapped movables to the trapped list and removes them from the movables
		list if there's no chance their partner will find them.

		:param movable: The trapped movable.
		"""
		self.trapped.append(movable)

		# If the trapped movable's partner has no chance of reaching them (not at
		# a direct neighbour station that was just closed) we can remove them
		# for a performance boost. We leave the partner in the game purely
		# trapped stats purposes, as they could become trapped somewhere else.
		if movable.partner.station not in self.tube_map.neighbors(movable.station):
			self.movables.remove(movable)

	def _handle_reunited(self, movable):
		"""
		Adds the reunited movable to the reunited list, removes the movable and partner from the movables list
		and closes the station they're located at.

		:param movable: The movable to check if it's reunited with its partner
		"""
		# Close the station as it's now full of lurve
		movable.station.open = False

		# Add the movable and its partner as a tuple to the reunited list
		self.reunited.append(Reunited(movable, movable.partner, self.ticks))

		# Remove the movables from the movables list as they're now out of the game
		self.movables.remove(movable)
		self.movables.remove(movable.partner)

		# Create the output according to the spec
		owner = movable if isinstance(movable, Owner) else movable.partner
		cat = movable if isinstance(movable, Cat) else movable.partner

		print "Owner {owner_name} ({owner_id}) found cat {cat_name} ({cat_id}) - {station_name} is now closed.".format(
			owner_name=owner.name,
			owner_id=owner.id,
			cat_name=cat.name,
			cat_id=cat.id,
			station_name=owner.station.name
		)
