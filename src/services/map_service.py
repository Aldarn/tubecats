#!/usr/bin/env python

import json

from ..models.station import Station
from graph_service import graph_service


class MapService(object):
	"""
	Encapsulates methods responsible for loading and building the tube map.
	"""
	station_json_keys = {
		"id": 0,
		"name": 1
	}

	connection_json_keys = {
		"from_station_id": 0,
		"to_station_id": 1
	}

	def __init__(self):
		pass

	def _get_data_value(self, data_set, keys, key):
		"""
		Allows human readable access to values in a data set by representing
		indexes as strings.

		:param data_set: The data set containing the values.
		:param keys: The human readable keys and their associated indices available for this data set.
		:param key: The human readable key to access.
		:return: The value corresponding to the requested key.
		"""
		return data_set[keys[key]]

	def _get_station_value(self, station_data, key):
		"""
		Gets the station data value for the given key.

		:param station_data: The station data.
		:param key: The key to access.
		:return: The value corresponding to the requested key.
		"""
		return self._get_data_value(station_data, self.station_json_keys, key)

	def _get_connection_value(self, connection_data, key):
		"""
		Gets the connection data value for the given key.

		:param connection_data: The connection data.
		:param key: The key to access.
		:return: The value corresponding to the requested key.
		"""
		return self._get_data_value(connection_data, self.connection_json_keys, key)

	def _load_stations(self, stations_json_file_path):
		"""
		Loads the stations from the given json file.

		:param stations_json_file_path: Path to the stations json file.
		:return: List of station objects.
		"""
		# Open the stations json file and load the json
		with open(stations_json_file_path, 'r') as stations_file:
			stations = json.load(stations_file)

		# Generate a list of station name labels
		labels = self._get_station_labels(stations)

		# Return a dictionary of stations indexed by their id
		return {
			self._get_station_value(station_data, "id"):
				Station(
					self._get_station_value(station_data, "id"),
					self._get_station_value(station_data, "name"),
					labels[self._station_name_label_exceptions(self._get_station_value(station_data, "name"))]
				)
			for station_data in stations
		}

	def _load_connections(self, stations, connections_json_file_path):
		"""
		Loads the connections from the given json file.

		:param stations: The station objects to connect.
		:param connections_json_file_path: Path to the connections json file.
		:return: List of lists of stations representing connections.
		"""
		# Open the connections json file and load the json
		with open(connections_json_file_path, 'r') as connections_file:
			connections = json.load(connections_file)

		# Map the connection ids to station objects and return the list
		return map(
			lambda connection: [
				stations[self._get_connection_value(connection, "from_station_id")],
				stations[self._get_connection_value(connection, "to_station_id")]
			],
			connections
		)

	def load_map(self, stations_json_file_path, connections_json_file_path):
		"""
		Loads the stations and connections into a graph representing the map.

		:param stations_json_file_path: File path to the connections json file.
		:param connections_json_file_path: File path to the station json file.
		:return: Graph representing the tube map.
		"""
		stations = self._load_stations(stations_json_file_path)
		connections = self._load_connections(stations, connections_json_file_path)

		return graph_service.build_graph(connections)

	def _station_name_label_exceptions(self, name):
		"""
		Mapping method for some extra long station names that cause collisions,
		inflating the length of the labels.

		:param name: The station name.
		:return: The shortened name, if applicable.
		"""
		name = name.replace(" ", "")  # Remove spaces for labels
		if name.startswith("HeathrowTerminal"):
			return "HTerm{number}".format(
				number=name.split("HeathrowTerminal")[1]
			)
		return name

	def _get_station_labels(self, stations):
		"""
		Gets a set of labels representing the given set of station names.

		:param stations: Station data.
		:return: Set of labels.
		"""
		# Get the station names, some are handled manually to reduce long similarities
		station_names = map(
			self._station_name_label_exceptions,
			set(self._get_station_value(station_data, "name") for station_data in stations)
		)

		# If there's only one station name the label is just going to be the first character
		if len(station_names) == 1:
			return {station_names[0]: station_names[0][0]}

		def label_length_binary_search(name_count, low=1, high=None):
			"""
			Modified binary search to find the smallest label size possible for the
			given list of station names ensuring each label is unique and the same
			length as each other.

			This method is the result of a bit of off-topic experimentation to find
			the most efficient method of reducing a set of strings to a set of their
			shortest unique representations of equal length - i.e. labels.

			E.g.:

			{'hackney', 'barking', 'barbican'} => {'hack', 'bark', 'barb'}

			Two previous methods included:

			(1) Using the set comparison found in this binary search in a linear loop
			starting at label length 1.

			(2) Sorting the set of names and linearly looping through them, incrementing
			the label length each time the current and previous names clashed by the
			number of characters they clashed by.

			The binary search method was the best on average for the timeit's I ran from
			small sets of small strings to large sets of large strings. Happy to discuss
			this in more detail if anyone is interested.

			:param name_count: Number of station names.
			:param low: The lowest length to search.
			:param high: The highest length to search.
			:return: List of labels.
			"""
			# If low or high are below 1 then either the names list is empty or contains empty strings; a low value
			# of below 1 would yield empty strings which aren't useful as labels :)
			if low <= high < 1:
				return None

			# If low == high then there's no search required as the length is exact
			if low == high:
				return {name: name[:low] for name in station_names}

			while low < high:
				mid = (low + high) // 2

				# Get a dictionary of station names to labels of length corresponding to the search index (mid)
				labels = {name: name[:mid] for name in station_names}
				label_count = len(set(labels.values()))  # Count the number of unique labels

				# Get the set of labels of length corresponding to the search index - 1
				left_labels = {name[:mid - 1] for name in station_names}
				left_labels_count = len(left_labels)

				# If the number of labels is the same as the number of names we know that
				# each label is unique, we must then check if the number of labels of
				# 1 length lower also generates the same number of labels as names -
				# if it does we know that this length can be reduced further, if not
				# we know this is the smallest possible length such that each station
				# name has a unique label.
				if label_count == name_count and left_labels_count < name_count:
					return labels
				elif label_count < name_count:
					low = mid + 1
				else:
					high = mid
			return None

		# The maximum index in our search corresponds to the length of the longest station name
		max_len = max(map(len, station_names))

		return label_length_binary_search(len(station_names), high=max_len)


map_service = MapService()