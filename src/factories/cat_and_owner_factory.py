import random
import itertools
from faker import Faker

from ..models.cat import Cat
from ..models.owner import Owner


class CatAndOwnerFactory(object):
	"""
	Factory responsible for creating pairs of cats and owners and adding them to the tube map.
	"""

	def __init__(self, tube_map):
		self.tube_map = tube_map

	def create_cat_and_owner(self, id):
		"""
		Creates a cat and owner pair located at a random station different from one another,
		ensuring their owner and cat references are set correctly.

		:param id: The id of the cat and owner to create.
		:return: The new cat and owner objects.
		"""
		# Get all stations in the map graph
		stations = self.tube_map.nodes()

		# Choose two different random stations
		cat_station, owner_station = random.sample(stations, 2)

		# Create the cat and owner objects with random names at the chosen stations
		# Special case for Batman and Fluffy for multi-game stats!
		cat = Cat(id, Faker().name() if id != 1 else "Fluffy", cat_station)
		owner = Owner(id, Faker().name() if id != 1 else "Batman", owner_station)

		# Set the cats owner reference which will in turn set the owners cat reference
		cat.owner = owner

		# Return the new objects
		return cat, owner

	def create_cat_and_owners(self, num):
		"""
		Creates the specified number of pairs of cats and owners.

		:param num: The number of cats and owners to create.
		"""
		# Since create_cat_and_owner returns a tuple we must unpack them into a flat list
		return list(itertools.chain(*[self.create_cat_and_owner(n + 1) for n in range(num)]))
