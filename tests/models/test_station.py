from unittest import TestCase

from mockito import mock

from src.models.station import Station
from ..utils import TestMovable


class TestStation(TestCase):
	def setUp(self):
		self.station = Station(1, "Clapham", "CLP")

	def test_add_movable_updates_movable_station_reference_when_different(self):
		movable = TestMovable(None)
		# --------------------------------------------------
		self.station.add_movable(movable)
		# --------------------------------------------------
		self.assertEquals(movable.station, self.station)

	def test_add_movable_doesnt_update_movable_owner_reference_when_same(self):
		class MockStation(mock):
			def __ne__(self, other):
				return False
		mock_station = MockStation()

		mock_movable = mock()
		mock_movable.station = mock_station
		# --------------------------------------------------
		self.station.add_movable(mock_movable)
		# --------------------------------------------------
		self.assertNotEquals(mock_movable.station.name, self.station.name)

	def test_remove_movable_sets_movable_station_to_none(self):
		movable = TestMovable(self.station)
		self.station.add_movable(movable)
		self.assertEquals(movable.station, self.station)
		# --------------------------------------------------
		self.station.remove_movable(movable)
		# --------------------------------------------------
		self.assertIsNone(movable.station)
