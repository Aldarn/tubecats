from unittest import TestCase

from networkx import Graph
from mockito import mock

from src.models.cat import Cat
from src.models.station import Station
from src.models.owner import Owner
from src.services.map_service import map_service
from resources.resources import getResourcePath


class TestOwner(TestCase):
	def setUp(self):
		self.station = Station(1, "Clapham", "CLP")
		self.cat = Cat(1, "Fluffy", self.station)
		self.owner = Owner(1, "Ben", self.station)

	def test_set_cat_updates_cat_owner_reference_when_different(self):
		self.assertIsNone(self.cat.owner)
		# --------------------------------------------------
		self.owner.cat = self.cat
		# --------------------------------------------------
		self.assertEquals(self.cat.owner, self.owner)

	def test_set_cat_doesnt_update_cat_owner_reference_when_same(self):
		class MockOwner(mock):
			def __ne__(self, other):
				return False
		mock_owner = MockOwner()

		mock_cat = mock()
		mock_cat.owner = mock_owner
		# --------------------------------------------------
		self.owner.cat = mock_cat
		# --------------------------------------------------
		self.assertNotEquals(mock_cat.owner.name, self.owner.name)

	def test_get_open_neighbors(self):
		station2 = Station(2, "Balham", "BAL")
		graph = Graph()
		graph.add_edges_from([(self.station, station2)])
		# --------------------------------------------------
		open_neighbors = self.owner._get_open_neighbors(graph)
		# --------------------------------------------------
		self.assertEquals(len(open_neighbors), 1)

	def test_get_open_neighbors_no_neighbors(self):
		station2 = Station(2, "Balham", "BAL")
		station2.open = False
		graph = Graph()
		graph.add_edges_from([(self.station, station2)])
		# --------------------------------------------------
		open_neighbors = self.owner._get_open_neighbors(graph)
		# --------------------------------------------------
		self.assertEquals(len(open_neighbors), 0)

	def test_get_new_neighbors(self):
		station2 = Station(2, "Balham", "BAL")
		graph = Graph()
		graph.add_edges_from([(self.station, station2)])
		# --------------------------------------------------
		open_neighbors = self.owner._get_new_neighbors(self.owner._get_open_neighbors(graph))
		# --------------------------------------------------
		self.assertEquals(len(open_neighbors), 1)

	def test_get_new_neighbors_no_new_neighbors(self):
		station2 = Station(2, "Balham", "BAL")
		graph = Graph()
		graph.add_edges_from([(self.station, station2)])
		self.owner._visited_stations.add(station2)
		# --------------------------------------------------
		open_neighbors = self.owner._get_new_neighbors(self.owner._get_open_neighbors(graph))
		# --------------------------------------------------
		self.assertEquals(len(open_neighbors), 0)

	def test_move_one_station(self):
		connected_station = Station(2, "Balham", "BAL")
		graph = Graph()
		graph.add_edges_from([(self.station, connected_station)])
		# --------------------------------------------------
		successful_move = self.owner.move(graph)
		# --------------------------------------------------
		self.assertTrue(successful_move)
		self.assertEquals(self.owner.station, connected_station)

	def test_move_respects_station_open_status(self):
		connected_station = Station(2, "Balham", "BAL")
		connected_station.open = False
		graph = Graph()
		graph.add_edges_from([(self.station, connected_station)])
		# --------------------------------------------------
		successful_move = self.owner.move(graph)
		# --------------------------------------------------
		self.assertFalse(successful_move)
		self.assertEquals(self.owner.station, self.station)

	def test_move_respects_station_open_status_mid_new_station_loop(self):
		station2 = Station(2, "Balham", "BAL")
		station3 = Station(3, "Tooting", "TOO")
		graph = Graph()
		graph.add_edges_from([(self.station, station2), (self.station, station3)])
		# --------------------------------------------------
		self.assertTrue(self.owner.move(graph))
		previous_random_station = self.owner.station
		self.assertTrue(self.owner.move(graph))
		self.assertEquals(self.owner.station, self.station)
		next_station = station2 if station2 != previous_random_station else station3
		next_station.open = False
		self.assertTrue(self.owner.move(graph))
		self.assertEquals(self.owner.station, previous_random_station)
		self.assertEquals(len(self.owner._visited_stations), 2)

	def test_move_backtracks_once_no_new_stations(self):
		station2 = Station(2, "Balham", "BAL")
		station3 = Station(3, "Tooting", "TOO")
		graph = Graph()
		graph.add_edges_from([(self.station, station2), (station2, station3)])
		# --------------------------------------------------
		self.assertTrue(self.owner.move(graph))
		self.assertEquals(self.owner.station, station2)
		self.assertTrue(self.owner.move(graph))
		self.assertEquals(self.owner.station, station3)
		self.assertTrue(self.owner.move(graph))
		self.assertEquals(self.owner.station, station2)
		self.assertTrue(self.owner.move(graph))
		self.assertEquals(self.owner.station, self.station)

	def test_move_backtrack_respects_open_status(self):
		station2 = Station(2, "Balham", "BAL")
		station3 = Station(3, "Tooting", "TOO")
		graph = Graph()
		graph.add_edges_from([(self.station, station2), (station2, station3)])
		# --------------------------------------------------
		self.assertTrue(self.owner.move(graph))
		self.assertEquals(self.owner.station, station2)
		self.assertTrue(self.owner.move(graph))
		self.assertEquals(self.owner.station, station3)
		successful_move = station2.open = False
		self.owner.move(graph)
		# --------------------------------------------------
		self.assertFalse(successful_move)
		self.assertEquals(self.owner.station, station3)

	def test_move_visits_all_stations(self):
		station2 = Station(2, "Balham", "BAL")
		station3 = Station(3, "Tooting", "TOO")
		graph = Graph()
		graph.add_edges_from([(self.station, station2), (self.station, station3)])
		# --------------------------------------------------
		self.assertTrue(self.owner.move(graph))
		previous_random_station = self.owner.station
		self.assertTrue(self.owner.move(graph))
		self.assertEquals(self.owner.station, self.station)
		self.assertTrue(self.owner.move(graph))
		self.assertEquals(self.owner.station, station2 if station2 != previous_random_station else station3)
		self.assertEquals(len(self.owner._visited_stations), 3)

	def test_move_count(self):
		station2 = Station(2, "Balham", "BAL")
		station3 = Station(3, "Tooting", "TOO")
		graph = Graph()
		graph.add_edges_from([(self.station, station2), (self.station, station3)])
		# --------------------------------------------------
		for i in range(3):
			self.owner.move(graph)
		# --------------------------------------------------
		self.assertEquals(self.owner.move_count, 3)

	def test_move_visits_all_tube_stations(self):
		tube_map = map_service.load_map(
			getResourcePath('tfl_stations.json'),
			getResourcePath('tfl_connections.json')
		)
		owner = Owner(1, "Tube Fanatic", tube_map.nodes()[100])
		# --------------------------------------------------
		for i in range(10000):
			if len(owner._visited_stations) == tube_map.number_of_nodes():
				break
			owner.move(tube_map)
		# --------------------------------------------------
		self.assertEquals(len(owner._visited_stations), tube_map.number_of_nodes())

	def test_move_moves_after_visiting_all_stations(self):
		station2 = Station(2, "Balham", "BAL")
		station3 = Station(3, "Tooting", "TOO")
		graph = Graph()
		graph.add_edges_from([(self.station, station2), (self.station, station3)])
		# --------------------------------------------------
		for i in range(5):
			self.owner.move(graph)
		# --------------------------------------------------
		self.assertEquals(self.owner.move_count, 5)
		self.assertNotEquals(self.owner.station, self.station)
