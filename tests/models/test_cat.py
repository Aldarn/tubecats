from unittest import TestCase

from mockito import mock
from networkx import Graph

from src.models.cat import Cat
from src.models.station import Station
from src.models.owner import Owner


class TestCat(TestCase):
	def setUp(self):
		self.station = Station(1, "Clapham", "CLP")
		self.cat = Cat(1, "Fluffy", self.station)
		self.owner = Owner(1, "Ben", self.station)

	def test_set_owner_updates_owner_cat_reference_when_different(self):
		self.assertIsNone(self.owner.cat)
		# --------------------------------------------------
		self.cat.owner = self.owner
		# --------------------------------------------------
		self.assertEquals(self.owner.cat, self.cat)

	def test_set_owner_doesnt_update_owner_cat_reference_when_same(self):
		class MockCat(mock):
			def __ne__(self, other):
				return False
		mock_cat = MockCat()

		mock_owner = mock()
		mock_owner.cat = mock_cat
		# --------------------------------------------------
		self.cat.owner = mock_owner
		# --------------------------------------------------
		self.assertNotEquals(mock_owner.cat.name, self.cat.name)

	def test_move(self):
		connected_station = Station(2, "Balham", "BAL")
		graph = Graph()
		graph.add_edges_from([(self.station, connected_station)])
		# --------------------------------------------------
		successful_move = self.cat.move(graph)
		# --------------------------------------------------
		self.assertTrue(successful_move)
		self.assertEquals(self.cat.station, connected_station)

	def test_move_respects_station_open_status(self):
		connected_station = Station(2, "Balham", "BAL")
		connected_station.open = False
		graph = Graph()
		graph.add_edges_from([(self.station, connected_station)])
		# --------------------------------------------------
		successful_move = self.cat.move(graph)
		# --------------------------------------------------
		self.assertFalse(successful_move)
		self.assertEquals(self.cat.station, self.station)

	def test_move_count(self):
		station2 = Station(2, "Balham", "BAL")
		station3 = Station(3, "Tooting", "TOO")
		graph = Graph()
		graph.add_edges_from([(self.station, station2), (self.station, station3)])
		# --------------------------------------------------
		for i in range(3):
			self.cat.move(graph)
		# --------------------------------------------------
		self.assertEquals(self.cat.move_count, 3)
