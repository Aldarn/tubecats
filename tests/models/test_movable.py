from unittest import TestCase
from weakref import WeakSet

from mockito import mock, verify, times, when

from ..utils import TestMovable
from src.models.station import Station
from src.models.cat import Cat
from src.models.owner import Owner


class TestMovables(TestCase):
	def setUp(self):
		self.station = Station(1, "Clapham", "CLP")
		self.movable = TestMovable(None)

	def test_set_station_adds_movable_reference_to_station_when_not_already_referenced(self):
		self.assertEquals(len(self.station.movables), 0)
		# --------------------------------------------------
		self.movable.station = self.station
		# --------------------------------------------------
		self.assertEquals(self.station.movables.pop(), self.movable)

	def test_set_station_doesnt_add_movable_reference_to_station_when_already_referenced(self):
		station = mock(Station)
		when(station).add_movable(self.movable).thenReturn()
		station.movables = WeakSet({self.movable})
		# --------------------------------------------------
		self.movable.station = station
		# --------------------------------------------------
		verify(station, times(0)).add_movable(self.movable)

	def test_movable_reunited(self):
		owner = Owner("1", "Batman", self.station)
		cat = Cat("1", "Fluffy", self.station)
		cat.owner = owner
		# --------------------------------------------------
		reunited = cat.reunited
		# --------------------------------------------------
		self.assertTrue(reunited)

	def test_movable_not_reunited(self):
		owner = Owner("1", "Batman", self.station)
		cat = Cat("1", "Fluffy", Station("2", "Edgeware", "E"))
		cat.owner = owner
		# --------------------------------------------------
		reunited = cat.reunited
		# --------------------------------------------------
		self.assertFalse(reunited)
