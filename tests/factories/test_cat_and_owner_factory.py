from unittest import TestCase

from mockito import when, verify, times, any
from networkx import Graph

from src.factories.cat_and_owner_factory import CatAndOwnerFactory
from src.models.station import Station


class TestCatAndOwnerFactory(TestCase):
	def setUp(self):
		tube_map = Graph()
		tube_map.add_nodes_from([Station("1", "Clapham South", "Cl"), Station("2", "Croydon", "Cr")])
		self.factory = CatAndOwnerFactory(tube_map)

	def test_create_cat_and_owners(self):
		when(self.factory).create_cat_and_owner(any()).thenReturn((None, None))
		# --------------------------------------------------
		cat_and_owners = self.factory.create_cat_and_owners(2)
		# --------------------------------------------------
		self.assertEquals(len(cat_and_owners), 4)
		verify(self.factory, times(2)).create_cat_and_owner(any())

	def test_create_cat_and_owner(self):
		# --------------------------------------------------
		cat, owner = self.factory.create_cat_and_owner(2)
		# --------------------------------------------------
		self.assertNotEquals(cat.station, owner.station)
		self.assertEquals(cat.owner, owner)
		self.assertEquals(owner.cat, cat)
		self.assertNotEquals(cat.name, "Fluffy")
		self.assertNotEquals(owner.name, "Batman")

	def test_create_fluffy_and_batman(self):
		# --------------------------------------------------
		cat, owner = self.factory.create_cat_and_owner(1)
		# --------------------------------------------------
		self.assertNotEquals(cat.station, owner.station)
		self.assertEquals(cat.owner, owner)
		self.assertEquals(owner.cat, cat)
		self.assertEquals(cat.name, "Fluffy")
		self.assertEquals(owner.name, "Batman")
