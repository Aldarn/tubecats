import sys
import cStringIO
from unittest import TestCase
from mock import patch, mock_open
from mockito import when, verify, any, times, mock

from src.models.reunited import Reunited
from src.models.cat import Cat
from src.models.owner import Owner
from src.models.station import Station
from src.services.stats_service import StatsService


class TestStatsService(TestCase):
	def setUp(self):
		self.service = StatsService()

	def test_save_stats_no_global_stats_creates_stats(self):
		import json
		when(json).dump(any(), any()).thenReturn(None)
		when(self.service)._load_stats().thenReturn(None)

		stats = {"success": True}
		with patch('__builtin__.open', mock_open(), create=True):
			# --------------------------------------------------
			self.service._save_stats(stats)
			# --------------------------------------------------
		verify(json).dump({"success_count": 1, "failure_count": 0}, any())

	def test_save_stats_global_stats_updates_stats(self):
		import json
		when(json).dump(any(), any()).thenReturn(None)

		global_stats = {
			"movables_count": 1,
			"reunited_count": 2,
			"average_find_moves": 3,
			"trapped_count": 4,
			"game_ticks": 5,
			"success_count": 6,
			"failure_count": 7
		}
		when(self.service)._load_stats().thenReturn(global_stats)

		stats = {
			"movables_count": 1,
			"reunited_count": 2,
			"average_find_moves": 9,
			"trapped_count": 4,
			"game_ticks": 5,
			"success": True
		}
		with patch('__builtin__.open', mock_open(), create=True):
			# --------------------------------------------------
			self.service._save_stats(stats)
			# --------------------------------------------------
		verify(json).dump({
			"movables_count": 2,
			"reunited_count": 4,
			"average_find_moves": 6,
			"trapped_count": 8,
			"game_ticks": 10,
			"success_count": 7,
			"failure_count": 7
		}, any())

	def test_load_stats_empty(self):
		with patch('src.services.stats_service.touch_open', mock_open(), create=True):
			# --------------------------------------------------
			stats = self.service._load_stats()
			# --------------------------------------------------
		self.assertEquals(stats, None)

	def test_load_stats(self):
		data = "{\"stat\": 1}"
		with patch('src.services.stats_service.touch_open', mock_open(read_data=data), create=True):
			# --------------------------------------------------
			stats = self.service._load_stats()
			# --------------------------------------------------
		self.assertEquals(stats, {"stat": 1})

	def test_generate_game_stats_saves_stats(self):
		when(self.service)._save_stats(any()).thenReturn(None)
		# --------------------------------------------------
		stats = self.service._generate_game_stats(1, [], [], 1)
		# --------------------------------------------------
		verify(self.service, times(1))._save_stats(any())

	def test_generate_game_stats_handles_empty_reunited_list(self):
		when(self.service)._save_stats(any()).thenReturn(None)
		# --------------------------------------------------
		stats = self.service._generate_game_stats(1, [], [], 1)
		# --------------------------------------------------
		self.assertEquals(stats["average_find_moves"], -1)

	def test_generate_game_stats_correct_stats(self):
		when(self.service)._save_stats(any()).thenReturn(None)

		cat1 = Cat(1, "Cat", Station(1, "Clapham", "CLP"))
		cat1.move_count = 5
		owner1 = Owner(1, "Owner", Station(2, "Balham", "BAL"))
		owner1.move_count = 5
		reunited1 = Reunited(cat1, owner1, 1)

		cat2 = Cat(2, "Cat2", Station(3, "Tooting", "TOOT"))
		cat2.move_count = 20
		owner2 = Owner(2, "Owner2", Station(4, "Morden", "MORDE"))
		owner2.move_count = 10
		reunited2 = Reunited(cat2, owner2, 1)
		# --------------------------------------------------
		stats = self.service._generate_game_stats(2, [reunited1, reunited2], [None], 1)
		# --------------------------------------------------
		self.assertEquals(stats["movables_count"], 2)
		self.assertEquals(stats["reunited_count"], 2)
		self.assertEquals(stats["average_find_moves"], 20)
		self.assertEquals(stats["trapped_count"], 1)
		self.assertEquals(stats["game_ticks"], 1)
		self.assertEquals(stats["success"], True)

	def test_generate_game_stats_failure_stat(self):
		when(self.service)._save_stats(any()).thenReturn(None)
		# --------------------------------------------------
		stats = self.service._generate_game_stats(1, [], [], 1)
		# --------------------------------------------------
		self.assertEquals(stats["success"], False)

	def test_output_stats(self):
		when(self.service)._generate_game_stats(any(), any(), any(), any()).thenReturn({
			"movables_count": 1,
			"reunited_count": 2,
			"average_find_moves": 3,
			"trapped_count": 4,
			"game_ticks": 5,
			"success": True
		})
		when(self.service)._load_stats().thenReturn({
			"movables_count": 1,
			"reunited_count": 2,
			"average_find_moves": 3,
			"trapped_count": 4,
			"game_ticks": 5,
			"success_count": 6,
			"failure_count": 7
		})
		stdout_ = sys.stdout
		stream = cStringIO.StringIO()
		sys.stdout = stream
		# --------------------------------------------------
		self.service.output_stats(1, [], [], 1)
		stats_output = stream.getvalue()
		# --------------------------------------------------
		sys.stdout = stdout_
		self.assertEquals(
			stats_output,
			"""
-------------------- Game Results - SUCCESS --------------------

Total number of cats: 1
Number of cats found: 2
Average number of movements required to find a cat: 3
Trapped cats and owners: 4
Game ticks: 5

----------------------------------------------------------------


----------------------- All Game Results -----------------------


SUCCESS 6 vs 7 FAILURE
Total number of cats: 1
Number of cats found: 2
Average number of movements required to find a cat: 3
Trapped cats and owners: 4
Game ticks: 5

----------------------------------------------------------------
"""
		)
		verify(self.service, times(1))._generate_game_stats(any(), any(), any(), any())
		verify(self.service, times(1))._load_stats()
