import json
from mock import patch, MagicMock, mock_open
from unittest import TestCase

from mockito import mock, when, verify, times, any

from resources.resources import getResourcePath
from src.services.map_service import MapService
from src.services.graph_service import GraphService
from src.models.station import Station


class TestMapService(TestCase):
	def setUp(self):
		self.map_service = MapService()
		self.stations_path = getResourcePath('tfl_stations.json')
		self.connections_path = getResourcePath('tfl_connections.json')

	def test_load_map(self):
		import src.services.map_service as map_service_module
		old_graph_service = map_service_module.graph_service
		map_service_module.graph_service = mock(GraphService)

		try:
			stations = [1, 2, 3]
			connections = [(1,2), (2,3)]

			when(self.map_service)._load_stations(self.stations_path).thenReturn(stations)
			when(self.map_service)._load_connections(stations, self.connections_path).thenReturn(connections)
			when(map_service_module.graph_service).build_graph(connections).thenReturn(None)
			# --------------------------------------------------
			graph = self.map_service.load_map(self.stations_path, self.connections_path)
			# --------------------------------------------------
			verify(self.map_service, times(1))._load_stations(self.stations_path)
			verify(self.map_service, times(1))._load_connections(stations, self.connections_path)
			verify(map_service_module.graph_service, times(1)).build_graph(connections)
			self.assertIsNone(graph)
		finally:
			map_service_module.graph_service = old_graph_service

	def _get_number_of_connections(self):
		"""
		It appears the connections file contains duplicates of some stations
		pointing to each other in reverse order - to get a real number of
		distinct connections so we can test the graph loaded correctly we
		must first count the number of distinct connections. Since the tube
		is an undirected graph it doesn't matter if a station is on the left
		or right hand side of an edge, this is the same connection.

		:return: Total number of distinct connections.
		"""
		with open(getResourcePath('tfl_connections.json'), 'r') as connections_file:
			connections = json.load(connections_file)
		distinct_connections = set()
		for connection in connections:
			if (connection[0], connection[1]) not in distinct_connections and (connection[1], connection[0]) not in distinct_connections:
				distinct_connections.add((connection[0], connection[1]))
		return len(distinct_connections)

	def test_load_map_with_tube_data(self):
		# --------------------------------------------------
		graph = self.map_service.load_map(self.stations_path, self.connections_path)
		# --------------------------------------------------
		self.assertEquals(graph.number_of_nodes(), 302)
		self.assertEquals(graph.size(), self._get_number_of_connections())

	def test_load_stations(self):
		when(self.map_service)._station_name_label_exceptions(any()).thenReturn("Acton Town")
		when(self.map_service)._get_station_labels(any()).thenReturn({"Acton Town": "ACT"})
		with patch('__builtin__.open', mock_open(read_data="[[\"1\", \"Acton Town\"]]"), create=True):
			# --------------------------------------------------
			stations = self.map_service._load_stations(self.stations_path)
			# --------------------------------------------------
		self.assertEquals(stations.keys()[0], "1")
		self.assertEquals(stations.values()[0].id, "1")
		self.assertEquals(stations.values()[0].name, "Acton Town")

	def test_load_connections(self):
		stations = {
			"1": Station("1", "First", "F"),
			"2": Station("2", "Second", "S")
		}
		with patch('__builtin__.open', mock_open(read_data="[[\"1\", \"2\"]]"), create=True):
			# --------------------------------------------------
			connections = self.map_service._load_connections(stations, self.connections_path)
			# --------------------------------------------------
		self.assertEquals(len(connections), 1)
		self.assertIn(stations["1"], connections[0])
		self.assertIn(stations["2"], connections[0])

	def test_get_station_labels_no_collisions(self):
		# --------------------------------------------------
		labels = self.map_service._get_station_labels([["1", "Acton Town"], ["2", "Clapham South"]])
		# --------------------------------------------------
		self.assertEquals(labels["ActonTown"], "A")
		self.assertEquals(labels["ClaphamSouth"], "C")

	def test_get_station_labels_single_name(self):
		# --------------------------------------------------
		labels = self.map_service._get_station_labels([["1", "Acton Town"]])
		# --------------------------------------------------
		self.assertEquals(labels["ActonTown"], "A")

	def test_get_station_labels_single_letter_names(self):
		# --------------------------------------------------
		labels = self.map_service._get_station_labels([["1", "A"], ["2", "B"]])
		# --------------------------------------------------
		self.assertEquals(labels["A"], "A")
		self.assertEquals(labels["B"], "B")

	def test_get_station_labels_collisions(self):
		# --------------------------------------------------
		labels = self.map_service._get_station_labels([["1", "High Barnet"], ["2", "Highgate"], ["3", "Clapham South"]])
		# --------------------------------------------------
		self.assertEquals(labels["HighBarnet"], "HighB")
		self.assertEquals(labels["Highgate"], "Highg")
		self.assertEquals(labels["ClaphamSouth"], "Claph")
