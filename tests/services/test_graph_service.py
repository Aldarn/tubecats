from unittest import TestCase

from src.services.graph_service import graph_service
from src.models.station import Station


class TestCat(TestCase):
	def test_build_graph(self):
		station1 = Station(1, "First", "F")
		station2 = Station(2, "Second", "S")
		# --------------------------------------------------
		graph = graph_service.build_graph([(station1, station2)])
		# --------------------------------------------------
		self.assertEquals(graph.number_of_nodes(), 2)
		self.assertEquals(graph.size(), 1)
