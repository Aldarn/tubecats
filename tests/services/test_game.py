from unittest import TestCase

from mockito import mock, when, verify, times, any

from src.services.game import Game
from src.services.stats_service import StatsService
from src.factories.cat_and_owner_factory import CatAndOwnerFactory
from src.models.station import Station
from ..utils import TestMovable


class GameTestCase(TestCase):
	def setUp(self):
		import src.services.game as game_module
		self.game_module = game_module

		# Mock the map service to return a mock tube map
		self.tube_map_mock = mock()
		when(game_module.map_service).load_map(any(), any()).thenReturn(self.tube_map_mock)

		# Mock the cat and owner factory to return mock movables
		self.cat_and_owner_mock = mock(CatAndOwnerFactory)

		self.movable_mock1 = mock(TestMovable)
		self.movable_mock1.station = mock(Station)
		self.movable_mock2 = mock(TestMovable)
		self.movable_mock2.station = mock(Station)
		self.movable_mock1.partner = self.movable_mock2
		when(self.movable_mock1).move(self.tube_map_mock).thenReturn(True)
		when(self.movable_mock2).move(self.tube_map_mock).thenReturn(True)

		movables = [self.movable_mock1, self.movable_mock2]
		when(self.cat_and_owner_mock).create_cat_and_owners(any()).thenReturn(movables)
		when(game_module).CatAndOwnerFactory(self.tube_map_mock).thenReturn(self.cat_and_owner_mock)

		# Mock stats service
		self.stats_service_mock = mock(StatsService)
		when(self.stats_service_mock).output_stats(any(), any(), any(), any()).thenReturn(None)
		when(game_module).StatsService().thenReturn(self.stats_service_mock)

		# Create the game inputs
		self.stations_json_file_path = "stations_json_file_path"
		self.connections_json_file_path = "connections_json_file_path"
		self.number_of_cats_and_owners = 5

		# Create the game service
		self.game = Game(
			self.stations_json_file_path,
			self.connections_json_file_path,
			self.number_of_cats_and_owners
		)

class TestGameInit(GameTestCase):
	def test_loads_tube_map_and_movables(self):
		verify(self.game_module.map_service, times(1)).load_map(
			self.stations_json_file_path,
			self.connections_json_file_path
		)
		verify(self.cat_and_owner_mock, times(1)).create_cat_and_owners(
			self.number_of_cats_and_owners
		)


class TestGameLogic(GameTestCase):
	def test_run_outputs_stats(self):
		self.game.MAX_TICKS = 1

		when(self.game)._handle_reunited(any()).thenReturn(None)
		# --------------------------------------------------
		self.game.run()
		# --------------------------------------------------
		verify(self.stats_service_mock, times(1)).output_stats(any(), any(), any(), any())

	def test_run_respects_max_ticks(self):
		self.game.MAX_TICKS = 2

		when(self.game)._handle_reunited(any()).thenReturn(None)
		# --------------------------------------------------
		self.game.run()
		# --------------------------------------------------
		self.assertEquals(self.game.ticks, self.game.MAX_TICKS)

	def test_run_moves_movables(self):
		self.game.MAX_TICKS = 1

		when(self.game)._handle_reunited(any()).thenReturn(None)
		self.movable_mock1.reunited = False
		self.movable_mock2.reunited = False
		# --------------------------------------------------
		self.game.run()
		# --------------------------------------------------
		verify(self.movable_mock1, times(1)).move(self.tube_map_mock)
		verify(self.movable_mock2, times(1)).move(self.tube_map_mock)

	def test_run_trapped(self):
		self.game.MAX_TICKS = 1

		when(self.game)._handle_reunited(any()).thenReturn(None)
		when(self.game)._handle_trapped(any()).thenReturn(None)
		when(self.movable_mock1).move(self.tube_map_mock).thenReturn(False)
		when(self.movable_mock2).move(self.tube_map_mock).thenReturn(False)
		self.movable_mock1.reunited = False
		self.movable_mock2.reunited = False
		# --------------------------------------------------
		self.game.run()
		# --------------------------------------------------
		verify(self.game, times(2))._handle_trapped(any())

	def test_run_reunited(self):
		self.game.MAX_TICKS = 1

		when(self.game)._handle_reunited(any()).thenReturn(None)
		self.movable_mock1.reunited = True
		self.movable_mock2.reunited = True
		# --------------------------------------------------
		self.game.run()
		# --------------------------------------------------
		verify(self.game, times(1))._handle_reunited(any())

	def test_run_reunited_partner_not_moved(self):
		self.game.MAX_TICKS = 1

		when(self.game)._handle_reunited(any()).thenReturn(None)
		self.movable_mock1.reunited = True
		self.movable_mock2.reunited = True
		# --------------------------------------------------
		self.game.run()
		# --------------------------------------------------
		verify(self.game, times(1))._handle_reunited(any())
		verify(self.movable_mock1, times(1)).move(self.tube_map_mock)
		verify(self.movable_mock1.partner, times(0)).move(self.tube_map_mock)

	def test_run_no_reunited(self):
		self.game.MAX_TICKS = 1

		when(self.game)._handle_reunited(any()).thenReturn(None)
		self.movable_mock1.reunited = False
		self.movable_mock2.reunited = False
		# --------------------------------------------------
		self.game.run()
		# --------------------------------------------------
		verify(self.game, times(0))._handle_reunited(any())

	def test_handle_reunited_closes_station(self):
		# --------------------------------------------------
		self.game._handle_reunited(self.movable_mock1)
		# --------------------------------------------------
		self.assertFalse(self.movable_mock1.station.open)

	def test_handle_reunited_adds_to_reunited_list(self):
		# --------------------------------------------------
		self.game._handle_reunited(self.movable_mock1)
		# --------------------------------------------------
		reunited = self.game.reunited.pop()
		self.assertEquals(self.movable_mock1, reunited.movable)
		self.assertEquals(self.movable_mock2, reunited.movable_partner)

	def test_handle_reunited_removes_from_movable_list(self):
		# --------------------------------------------------
		self.game._handle_reunited(self.movable_mock1)
		# --------------------------------------------------
		self.assertEquals(len(self.game.movables), 0)

	def test_handle_trapped_partner_not_nearby(self):
		when(self.tube_map_mock).neighbors(self.movable_mock1.station).thenReturn([])
		# --------------------------------------------------
		self.game._handle_trapped(self.movable_mock1)
		# --------------------------------------------------
		self.assertEquals(len(self.game.trapped), 1)
		self.assertEquals(len(self.game.movables), 1)

	def test_handle_trapped_partner_nearby(self):
		when(self.tube_map_mock).neighbors(self.movable_mock1.station).thenReturn([self.movable_mock2.station])
		# --------------------------------------------------
		self.game._handle_trapped(self.movable_mock1)
		# --------------------------------------------------
		self.assertEquals(len(self.game.trapped), 1)
		self.assertEquals(len(self.game.movables), 2)
