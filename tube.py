#!/usr/bin/env python

import logging
import argparse
from src.services.game import Game


DESCRIPTION = "London Underground cat finder simulation"


def get_command_line_arguments():
	"""
	Manages command line arguments and information.

	:return: Object containing arguments and values.
	"""
	parser = argparse.ArgumentParser(description=DESCRIPTION)
	parser.add_argument(
		"-n",
		"--number-of-cats-and-owners",
		type=int,
		dest="number_of_cats_and_owners",
		default=10,
		help="The number of cats and owners to generate."
	)
	parser.add_argument(
		"-s",
		"--stations",
		type=str,
		dest="stations_json_file_path",
		default="resources/tfl_stations.json",
		help="The path to the stations json file."
	)
	parser.add_argument(
		"-c",
		"--connections",
		type=str,
		dest="connections_json_file_path",
		default="resources/tfl_connections.json",
		help="The path to the connections json file."
	)
	return parser.parse_args()


def main():
	# Get any command line arguments supplied or their defaults
	command_line_arguments = get_command_line_arguments()

	print "Welcome to the {description}!\n".format(description=DESCRIPTION)

	# Start the game
	game = Game(
		command_line_arguments.stations_json_file_path,
		command_line_arguments.connections_json_file_path,
		command_line_arguments.number_of_cats_and_owners
	)
	game.run()


if __name__ == '__main__':
	main()
